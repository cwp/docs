# CWP user help.

 * This is based off https://github.com/silverstripe/silverstripe-userhelp-content .
 * Technical CWP docs are tracked in https://gitlab.cwp.govt.nz/cwp/cwp/tree/master/docs/en
 * Generic docs (feature descriptions) are edited through the CMS on cwp.govt.nz
 * [Documentation guidelines](https://docs.google.com/document/d/1zhHbxDA5bloXwKjE4B5PrFuFQIz7p5BFK8ykFFQ38TI/edit#heading=h.d8ch9x9wgl9j)