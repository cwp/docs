<!--
pagenumber: 9
title: Writing content in multiple languages
-->

# Writing content in multiple languages

Please visit the [translatable documentation](https://www.cwp.govt.nz/guides/core-technical-documentation/translatable-module/en/).
