<!--
pagenumber: 14
title: Importing content from Microsoft Word
-->

# Importing content from Microsoft Word 

You can easily publish Microsoft Word documents to your website using an import feature built into the content management system (CMS). It shall auto-convert the document into HTML pages, and provides an efficient way to upload long documents or multiple webpages of content. Items like tables, images, heading levels, and links are kept, while fonts and visual styles are transformed to the relevant visual styles of your website.

To import a document, visit any page of your website inside the CMS, choose the Import tab, and use the form shown below:

![import document screenshot](_images/import-document-screenshot.jpg)
