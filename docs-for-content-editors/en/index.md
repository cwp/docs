<!--
title: For Content Editors
pagenumber: 3
-->

# For Content Editors

This section provides guides for Content Editors. Content Editors are responsible for authoring and/or publishing content to websites, and use an online software package known as a Content Management System (CMS) to carry out this role. 

Content Editors may be classified as either:

* Authors, able to write content to a draft area that is not publicly visible.
* Publishers, who can approve draft content and make it publicly available.

A Content Editor is expected to have a reasonable knowledge of best practices for writing plain english content for the web, and will need to become familiar with using SilverStripe CMS, the software used. A Content Editor does not need to be able to write HTML and CSS. 

A CMS Administrator has greater access to the features of the CMS and can create and remove Content Editor accounts. 

See also:

* [Roles and Responsibilities](https://www.cwp.govt.nz/guides/roles-and-responsibilities/)
* [Training](https://www.cwp.govt.nz/guides/instance-management/training-content-editors-and-developers/)

