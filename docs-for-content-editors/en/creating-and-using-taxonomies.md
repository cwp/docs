<!--
pagenumber: 13
title: Creating and using taxonomies
-->

# Creating and using taxonomies

Please visit the [taxonomy documentation](https://www.cwp.govt.nz/guides/core-technical-documentation/taxonomy-module/en/user/).
