<!--
pagenumber: 10
title: Using content workflows, embargo and expiry
-->

# Using content workflows, embargo and expiry

Please visit the [workflow documentation](https://github.com/silverstripe-australia/advancedworkflow/blob/master/HOWTO.md).
