<!--
title: Setting up and managing subsites
pagenumber: 5
-->

# Setting up and managing subsites

The silverstripe-subsites module allows you to manage multiple related sites through a single CMS interface. Because all
sites run on a single installation of SilverStripe, they can share users, content and assets. They can all use the same
templates, or each use different ones, but they are limited to using the same codebase (repository source).

1. [Setting up subsites](https://www.cwp.govt.nz/guides/core-technical-documentation/subsites-module/en/set_up)
1. [Working with subsites](https://www.cwp.govt.nz/guides/core-technical-documentation/subsites-module/en/working_with)

## Domains

Adding a new Subsite to the CWP instance requires it to first be configured in your CMS. Follow the
[Subsites documentation](https://www.cwp.govt.nz/guides/core-technical-documentation/subsites-module/en/set_up) for how
to set it up.

At minimum two domains need to be configured for the live site, and one for the UAT:

* "<whatever-you-like>-<instance-id>.cwp.govt.nz" - an internal testing domain.
* Your chosen production domain - only required for the production environment. This will be the domain name regular
visitors will use to visit the subsite.

After this has been done, new domains need to be configured in the CWP infrastructure to get your server to respond to
the subsite traffic going to these domains. This requires filing a Service Desk ticket with the list of necessary
domains to set up (the list should match the domains configured in your CMS).

Note that setting up of the production domain will require DNS changes through your domain registrar. We will advise the
necessary steps through the Service Desk ticket.
