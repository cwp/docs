<!--
title: Website administrators 
pagenumber: 4 
-->

# For website administrators

A CMS Administrator is someone who has full access to the administration interface of the Content Management System that powers a website. This access includes controlling the access rights of other people, such as Content Editors. A CMS Administrator also has access to various site settings and may be able to change certain types of information held on the website that regular Content Editors cannot.

See also:
[Roles and responsibilities](https://www.cwp.govt.nz/guides/roles-and-responsibilities/)


In this section, you can learn to:

* [Change and manage user accounts](changing-and-managing-users)
* [Manage roles and permissions](managing-roles-and-permissions)
