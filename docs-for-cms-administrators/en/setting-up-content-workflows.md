<!--
title: Setting up content workflows
pagenumber: 6
-->

Content workflow can be enabled to enforce that content changes go through approval processes before being published. For example, you might have a team of staff who author content, but they are not allowed to publish that content publicly until a manager or communications advisor has given approval.

The same system also allows you to set up embargo and expiry dates on your pages. This enables pages be assigned a time and date when the page will be automatically published to or removed from your website.

See also:

* This functionality is provided by the advanced workflow module, which has technical documentation at [https://github.com/silverstripe-australia/advancedworkflow/blob/master/HOWTO.md](https://github.com/silverstripe-australia/advancedworkflow/blob/master/HOWTO.md)
