<!--
title: Managing online databases and registries
pagenumber: 7
-->

# Managing online databases and registries

Websites running on the Common Web Platform can publish databases, not just traditional web pages. For example, you might publish a database of all of the schools in the country, or a registry of people holding a qualification, so that website visitors can find information they seek.

The built-in features:

* a public search form
* see data summarised in a list, and drill into detail on an item by item basis.
* export to CSV (and RESTFul API)
* CMS Administrators can bulk upload data via CSV or edit items one by one using an online form

A website developer is needed initially to create the “database” however subsequently it can be readily managed and interacted with using online tools as pictured below:

![Screenshot showing a software interface that allows how to manage a database of data, including a search form, and buttons to add, edit and remove data](_images/registry-screeenshot.jpg)

See also:

* This feature is provided by the Registry Module. Developer documentation is available at [https://www.cwp.govt.nz/guides/core-technical-documentation/registry-module/en](https://www.cwp.govt.nz/guides/core-technical-documentation/registry-module/en). The Registry Module in turn makes use of a technical feature of SilverStripe CMS named ModelAdmin (see [technical help on ModelAdmin](https://www.cwp.govt.nz/guides/core-technical-documentation/framework/en/reference/modeladmin)).
